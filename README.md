# Créer un projet

Ce projet a pour objectif de donner les principales étapes de la construction d'un site web à partir de fichiers sources se trouvant 
sur une machine personnelle.

On initialise un projet ``git`` avec es commandes suivantes dans le dossier à déposer:

	git init
	git config --local user.name "Nom Utilisateur"
	git config --local user.email "mon.email@exemple.com"

Lorsque le projet est prêt à un premier commit, on saisit les commandes suivantes:

	git add .
	git commit -m "message"
	git remote add origin https://oauth2:<jeton_acces>@forge.apps.education.fr/<utilisateur>/<nom_projet>.git
	git push -u origin master

Je fait une modification d'un fichier ou je crée un nouveau fichier. Je dois donc commiter vers le dépôt distant Gitlab.
Les commandes à saisir sont les suivantes:

	git add .
	git commit -m "modification ou ajout"
	git push origin master
